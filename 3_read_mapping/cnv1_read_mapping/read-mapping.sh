#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH --array=1-3
#SBATCH -p normal
#SBATCH --mem=200g
#SBATCH -n 4
#SBATCH -J read-mapping
#SBATCH -o read-mapping-%A_%a.out
# ----------------Load Modules--------------------
module load Bowtie2
module load SAMtools
# ----------------Commands------------------------
module list

filelist=(`ls /home/a-m/bzhao8/BZ-CNV1/analysis2/read-cleaning/*.fastq`)

input=${filelist[$SLURM_ARRAY_TASK_ID-1]}
echo $'\n'input = $input

libname=${input%%.*}

libname=${libname##*/}
echo libname = $libname

output1=${libname}.sam
echo output1 = $output1

output2=${libname}.bam
echo output2 = $output2

output3=${libname}.s.bam
echo output3 = $output3

output4=${libname}.sr.bam
echo output4 = $output4


echo start mapping by bowtie2, time: $(date)

bowtie2 -x /home/a-m/bzhao8/bowtie2-indexes/mm10tg2/mm10tg2 -U $input -S $output1 -p 4

echo $'\n'mapping finished, time: $(date)

echo $'\n'start cleaning up by samtools, time: $(date)

samtools view -b $output1 -o $output2 -@ 4

samtools sort $output2 -o $output3 -@ 4

samtools rmdup -s $output3 $output4

samtools index $output4 -@ 4

echo $'\n'cleaning up finished, time: $(date)
