#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH --array=1-6
#SBATCH -p normal
#SBATCH --mem=50g
#SBATCH -n 8
#SBATCH -J read-mapping
#SBATCH -o read-mapping-%A_%a.out
# ----------------Load Modules--------------------
module load Bowtie2
module load SAMtools
# ----------------Commands------------------------
module list

filelist=(`ls /igbgroup/a-m/bzhao8/BZ-CNV2/1read-cleaning/*.fastq`)

input=${filelist[$SLURM_ARRAY_TASK_ID-1]}
echo $'\n'input = $input

libname=${input%%.*}

libname=${libname##*/}
echo libname = $libname

output1=${libname}.sam
echo output1 = $output1

output2=${libname}.bam
echo output2 = $output2

output3=${libname}.s.bam
echo output3 = $output3

output4=${libname}.sr.bam
echo output4 = $output4


echo $'\n'start mapping by bowtie2, time: $(date)

bowtie2 -x /igbgroup/a-m/bzhao8/bowtie2-indexes/mm10tg2/mm10tg2 -U $input -S $output1 -p 8

echo $'\n'mapping finished, time: $(date)

echo $'\n'start cleaning up by samtools, time: $(date)

samtools view -b $output1 -o $output2 -@ 8

samtools sort $output2 -o $output3 -@ 8

samtools rmdup -s $output3 $output4

samtools index $output4 -@ 8

echo $'\n'cleaning up finished, time: $(date)
