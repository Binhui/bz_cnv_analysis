#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH -p normal
#SBATCH -c 1
#SBATCH --mem=50g
#SBATCH -N 1
#SBATCH -J build-mm10tg2-bowtie2
#SBATCH -o build-mm10tg2-bowtie2-slurm-%j.out
# ----------------Load Modules--------------------
module load Bowtie2/2.3.2-IGB-gcc-4.9.4
# ----------------Commands------------------------
chr=`ls /home/a-m/bzhao8/chromFa/mm10tg/*.fa | tr '\n' ','`
chr=${chr%?}
bowtie2-build -f $chr mm10tg2
