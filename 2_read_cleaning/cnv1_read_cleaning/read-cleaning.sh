#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH --array=1-3
#SBATCH -p normal
#SBATCH --mem=50g
#SBATCH -J read-cleaning
#SBATCH -o read-cleaning-%A_%a.out
# ----------------Load Modules--------------------
module load cutadapt/1.14-IGB-gcc-4.9.4-Python-2.7.13
module load FastQC/0.11.5-IGB-gcc-4.9.4-Java-1.8.0_152
# ----------------Commands------------------------
filelist=(`ls /home/a-m/bzhao8/BZ-CNV1/3T3-fastq/*`)

input=${filelist[$SLURM_ARRAY_TASK_ID-1]}

libname=${input%%_lib*}

libname=${libname##*/}

output=${libname}.aq.fastq

cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -q 20,20 -m 20 -o $output $input
fastqc -a /home/a-m/bzhao8/BZ-CNV1/analysis2/read-cleaning/test/adapter.txt $output $input
