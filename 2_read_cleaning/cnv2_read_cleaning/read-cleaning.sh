#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH --array=1-6
#SBATCH -p normal
#SBATCH --mem=50g
#SBATCH -J read-cleaning
#SBATCH -o read-cleaning-%A_%a.out
# ----------------Load Modules--------------------
module load cutadapt
module load FastQC
# ----------------Commands------------------------
echo module list:
module list

filelist=(`ls /igbgroup/a-m/bzhao8/BZ-CNV2/fastqs/*`)

input=${filelist[$SLURM_ARRAY_TASK_ID-1]}

libname=${input%%_lib*}

libname=${libname##*/}

output=${libname}.aq.fastq

cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -q 20,20 -m 20 -o $output $input
fastqc -a adapter.txt $output $input
