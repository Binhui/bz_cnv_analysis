for file in ../../read_counting_data/*/*.var.wig
do
    file_name=${file##*/}
    echo $file_name
    sed '/CTD/q' $file | sed '1d;$d' > m_$file_name
done
