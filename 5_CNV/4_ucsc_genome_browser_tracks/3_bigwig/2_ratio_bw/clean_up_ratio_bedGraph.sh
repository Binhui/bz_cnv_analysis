for file in ../../2_ratio_bedGraph/*.bedGraph
do
    file_name=${file##*/}
    echo $file_name
    file_name1=${file_name%.*}
    sort -k1,1 -k2,2n $file > cleaned_up/$file_name1.sorted.bedGraph
    ../bedClip.dms -truncate cleaned_up/$file_name1.sorted.bedGraph ../wigToBigWig/mm10.chrom.sizes cleaned_up/$file_name1.sorted.trimmed.bedGraph

done
