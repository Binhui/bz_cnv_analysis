for file in cleaned_up/*.trimmed.bedGraph
do
    file_name=${file##*/}
    echo $file_name
    file_name1=${file_name%.*}
    ../bedGraphToBigWig/bedGraphToBigWig.dms $file ../wigToBigWig/mm10.chrom.sizes $file_name1.bw
done
