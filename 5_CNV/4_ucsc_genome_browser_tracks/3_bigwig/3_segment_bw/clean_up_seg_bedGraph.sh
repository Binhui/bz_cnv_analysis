for file in ../../../3_analysis/analysis_output/*.bedGragh
do
    file_name=${file##*/}
    echo $file_name
    file_name1=${file_name%.*}
    grep -v "NA" $file | grep -v "track type" | sort -k1,1 -k2,2n > cleaned_up/$file_name1.sorted.bedGraph
    ../bedClip.dms -truncate cleaned_up/$file_name1.sorted.bedGraph ../wigToBigWig/mm10.chrom.sizes cleaned_up/$file_name1.sorted.trimmed.bedGraph

done
