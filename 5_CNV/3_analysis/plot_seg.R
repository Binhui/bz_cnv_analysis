rm(list = ls())
setwd('/Volumes/Macintosh HD/NGS/key_analysis_20190623/5_CNV/3_analysis/plot_seg')

library(ggplot2)
library(grid)
library(gridExtra)
library(stringr)
library(scales)
source('../fun_plot_ratios_vs_reads.R')





##### plot1 ######
mydata <- read.delim('../outputs_cal/high_labeled.txt', stringsAsFactors = F)





# order mydata
mydata$binsize <- factor(mydata$binsize, levels = c('3 kb bin', '30 kb bin'))


# plot
p1 <- plot_ratios_vs_reads(mydata[grepl('DHFR',mydata$ID),], a = c('mean_log2_reads', 'seg.mean'), l = c('mean log2(ratio)', 'mean log2(normalized reads)'))
p1 <- p1+xlim(-7,7)

p2 <- plot_ratios_vs_reads(mydata[grepl('HBB',mydata$ID),], a = c('mean_log2_reads', 'seg.mean'), l = c('mean log2(ratio)', 'mean log2(normalized reads)'))
p2 <-  p2+xlim(-7,7)




pdf('plot_ratio_vs_reads.pdf', width=6, height = 3)
grid.arrange(p1, p2, nrow=1)
dev.off()


sessionInfo()
# R version 3.6.0 (2019-04-26)
# Platform: x86_64-apple-darwin15.6.0 (64-bit)
# Running under: macOS High Sierra 10.13.6
# 
# Matrix products: default
# BLAS:   /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
# LAPACK: /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRlapack.dylib
# 
# locale:
#     [1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8
# 
# attached base packages:
#     [1] grid      parallel  stats4    stats     graphics  grDevices utils    
# [8] datasets  methods   base     
# 
# other attached packages:
#     [1] scales_1.0.0         gridExtra_2.3        DNAcopy_1.58.0      
# [4] stringr_1.4.0        ggplot2_3.2.0        rtracklayer_1.43.3  
# [7] GenomicRanges_1.36.0 GenomeInfoDb_1.20.0  IRanges_2.18.1      
# [10] S4Vectors_0.22.0     BiocGenerics_0.30.0 
# 
# loaded via a namespace (and not attached):
#     [1] Rcpp_1.0.1                  plyr_1.8.4                 
# [3] compiler_3.6.0              pillar_1.4.1               
# [5] BiocManager_1.30.4          XVector_0.24.0             
# [7] bitops_1.0-6                tools_3.6.0                
# [9] zlibbioc_1.30.0             digest_0.6.19              
# [11] tibble_2.1.3                gtable_0.3.0               
# [13] lattice_0.20-38             pkgconfig_2.0.2            
# [15] rlang_0.3.4                 Matrix_1.2-17              
# [17] DelayedArray_0.10.0         GenomeInfoDbData_1.2.1     
# [19] withr_2.1.2                 Biostrings_2.52.0          
# [21] Biobase_2.44.0              XML_3.98-1.20              
# [23] BiocParallel_1.18.0         reshape2_1.4.3             
# [25] magrittr_1.5                Rsamtools_2.0.0            
# [27] matrixStats_0.54.0          GenomicAlignments_1.20.1   
# [29] SummarizedExperiment_1.14.0 colorspace_1.4-1           
# [31] labeling_0.3                stringi_1.4.3              
# [33] RCurl_1.95-4.12             lazyeval_0.2.2             
# [35] munsell_0.5.0               crayon_1.3.4 
