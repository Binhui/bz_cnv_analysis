#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH --array=1-9
#SBATCH -p normal
#SBATCH --mem=50g
#SBATCH -J read_counting_cnv1-2_3kb
#SBATCH -o read_counting_cnv1-2_3k_%A_%a.out
# ----------------Load Modules--------------------
module load Python
# ----------------Commands------------------------
module list

filelist1=(`ls /igbgroup/a-m/bzhao8/BZ-CNV1/analysis2/read-mapping/*.sr.bam`)
filelist2=(`ls /igbgroup/a-m/bzhao8/BZ-CNV2/2read-mapping/*.sr.bam`)
filelist=("${filelist1[@]}" "${filelist2[@]}")

input=${filelist[$SLURM_ARRAY_TASK_ID-1]}

echo input = $input

python /igbgroup/a-m/bzhao8/BZ-CNV1/analysis2/read-counting/count_reads4.py -b 3000 -i $input
