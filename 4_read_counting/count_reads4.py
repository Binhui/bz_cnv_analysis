'''
Author: BZ
Most codes are borrowed from: 
https://github.com/ma-compbio/TSA-Seq-toolkit
Python3

'''

import pysam
import re
import math
import os
import sys, argparse

# parsing argument
def parse_argument():
    p = argparse.ArgumentParser(description = 'Example: %(prog)s -h', epilog = 'Library dependency : pysam')
    p.add_argument('-v','--version',action = 'version', version = '%(prog)s 0.4')
    p.add_argument('-b','--binsizes',dest = 'binsizes',type = int, nargs='+', default = [10000], help = 'bin sizes for counting reads')
    p.add_argument('-i','--input',dest = 'input_bam',type = str, help = 'input bam file', required = True)
    p.add_argument('-o','--output',dest = 'output_dir',type = str, help = 'output directory') 
    if len(sys.argv)<2:
        sys.exit(p.print_help())
    return p.parse_args()


# getting a dict of chr lengths and a dict of basic metrics
def get_metrics(bamfile):
    chr_dict = {}
    metrics = {}
    
    for i in range(len(bamfile.references)):
        chr_dict[bamfile.references[i]] = bamfile.lengths[i]

    metrics['mapped_reads'] = bamfile.mapped
    metrics['number_of_chromosomes'] = len(chr_dict.keys())
    metrics['genome_size'] = sum(chr_dict.values())
    
    return chr_dict, metrics

# counting reads in bins
def count_binned_reads(bamfile, binsize, mean_binned_reads, chr_dict):
    # initiate a dictionary to record the number of reads in each bin for each chromosome
    binned_reads = {}
    
    for c in chr_dict:
        bins = math.ceil(chr_dict[c]/binsize)
        binned_reads[c] = [0]*bins
        
    # calculate the number of reads in each bin
    processed_reads = 0
    for alignment in bamfile:
        if alignment.tid < 0:
            continue 
            
        chr_name = bamfile.get_reference_name(alignment.tid)        
        pos = (alignment.reference_start + alignment.reference_end)/2  
        binned_reads[chr_name][math.floor(pos/binsize)] += 1/mean_binned_reads
        
        processed_reads += 1
    
    print('processed_reads: %d' % processed_reads)
    return binned_reads

# writing binned_reads to a fixed-step wiggle file, converting values to log2 scale (0 -> m (log2 (1/mean_binned_reads))) 
def write_fixed_wig(output, binned_reads, binsize, m):
    fo = open(output,'w')
    # track line must be omitted for use with wigtobigwig program
    print ('track type=wiggle_0 name=%s autoScale=on' % os.path.basename(output), file=fo)
    for c in binned_reads:
        print ('fixedStep chrom=' + c + ' start=1'  + ' span=' + str(binsize) + ' step=' + str(binsize), file=fo)
        values = binned_reads[c]
        for v in values:
            if v == 0:
                log_v = math.log(m,2)
            else:
                log_v = math.log(v,2)
            print ('%.3f' % log_v, file=fo)
    fo.close()
    
# writing binned_reads to a variable-step wiggle file, converting values to log2 scale (0 removed) 
def write_var_wig(output, binned_reads, binsize):
    fo = open(output,'w')
    # track line must be omitted for use with wigtobigwig program
    print ('track type=wiggle_0 name=%s autoScale=on' % os.path.basename(output), file=fo)
    for c in binned_reads:
        print ('variableStep chrom=' + c + ' span=' + str(binsize), file=fo)
        values = binned_reads[c]
        for i in range(len(values)):
            if values[i] != 0:
                print ("%d\t%.3f" % (i*binsize+1, math.log(values[i],2)), file=fo) 
    fo.close()


# function for creating output paths
def get_output_path(input_bam, output_dir, binsize):
    wigfile = os.path.basename(input_bam)
    fixed_wig = wigfile[:wigfile.rfind('.')+1] + str(int(binsize/1000)) + 'kb_bin.fixed.wig'
    var_wig = wigfile[:wigfile.rfind('.')+1] + str(int(binsize/1000)) + 'kb_bin.var.wig'
    if output_dir != None:
        if not os.path.isdir(output_dir):
            print('making output directory: ' + output_dir)
            os.mkdir(output_dir)
        fixed_wig = os.path.join(output_dir, fixed_wig)
        var_wig = os.path.join(output_dir, var_wig)

    print('output path:', fixed_wig, var_wig, sep='\n')
    return fixed_wig,var_wig



    
########################################################

# Parse arguments
args = parse_argument()
input_bam = args.input_bam
output_dir = args.output_dir
binsizes = args.binsizes

print ('# Opening input bam file #')
print (input_bam)
try:
    bamfile = pysam.Samfile(input_bam,'rb')
except:
    sys.exit('input file does not exist')

# Getting bam alignment metrics
print ('# Getting alignment metrics #')

chr_dict, metrics = get_metrics(bamfile)

bamfile.close()

for m in metrics:
     print (m, ': ', metrics[m])


# Counting number of reads in bins
print ('# Counting number of reads in bins #')

for binsize in binsizes:
    
    bamfile = pysam.Samfile(input_bam,'rb')
    
    print('--bin size %d kb:' % int(binsize/1000))

    mean_binned_reads = metrics['mapped_reads'] * binsize / metrics['genome_size']

    print('mean_binned_reads = %.2f' % mean_binned_reads )

    binned_reads = count_binned_reads(bamfile, binsize, mean_binned_reads, chr_dict)

    bamfile.close()

    fixed_wig, var_wig =  get_output_path(input_bam, output_dir, binsize)
    
    write_fixed_wig(fixed_wig, binned_reads, binsize, 0.1/mean_binned_reads)

    write_var_wig(var_wig, binned_reads, binsize)


print ('# Analysis Done #')
