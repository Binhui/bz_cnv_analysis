#!/bin/bash
# ----------------SLURM Parameters----------------
#SBATCH --mail-user bzhao8@illinois.edu
#SBATCH --mail-type BEGIN,END,FAIL
#SBATCH --array=1-6
#SBATCH -p normal
#SBATCH --mem=50g
#SBATCH -J read-counting
#SBATCH -o read-counting-%A_%a.out
# ----------------Load Modules--------------------
module load Python
# ----------------Commands------------------------
module list

filelist=(`ls /igbgroup/a-m/bzhao8/BZ-CNV2/2read-mapping/*.sr.bam`)

input=${filelist[$SLURM_ARRAY_TASK_ID-1]}

echo input = $input

python count_reads4.py -b 100000 30000 15000 5000 -i $input
